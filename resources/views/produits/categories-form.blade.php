@extends('layout')

@section('container')
@if(isset($category))
{!! Form::model($category, ['url' => route('update-category', $category->id), 'files'=>true]) !!}
@else
{!! Form::open(['url' => route('save-category'), 'files'=>true]) !!}
@endif	
 	<h1>Ajouter ou modifier une catégorie</h1>
 	@if(isset($category))
 	<div class="img-theme"><img src="{{asset('../public/img/uploads/').'/'.$category->theme}}"></div>
 	@endif
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
		    <label>Nom de la catégrie</label>
		    {!! Form::text('titre', null, ['class' => 'form-control', 'required'=>true, 'placeholder'=>'Nom de la catégrie']) !!}
		  </div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
		    <label>Lien de la catégorie</label>
		    {!! Form::url('url', null, ['class' => 'form-control', 'required'=>true, 'placeholder'=>'Lien de la catégrie']) !!}
		  </div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
		    <label>Type</label>
		    {!! Form::select('type', $type_category, null, ['class' => 'form-control', 'required'=>true, 'placeholder'=>'Type de la catégrie']) !!}
		  </div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
		    <label>Photo thème</label>
		    <input type="file" name="theme" class="form-control">
	
		  </div>
		</div>

		
		<div class="col-md-12">
			<hr>
			<h4>Détail de la page produit</h4>
		</div>
		<div class="col-md-6">
			<div class="form-group">
		    <label>Class produit</label>
		    {!! Form::text('html', null, ['class' => 'form-control',  'placeholder'=>'Classname']) !!}
		  </div>
		</div>


		<div class="col-md-6">
			<div class="form-group">
		    <label>Class du nom du produit</label>
		    {!! Form::text('name', null, ['class' => 'form-control',  'placeholder'=>'Classname']) !!}
		  </div>
		</div>	


		<div class="col-md-6">
			<div class="form-group">
		    <label>Class de l'image du produit</label>
		    {!! Form::text('image', null, ['class' => 'form-control',  'placeholder'=>'Classname']) !!}
		  </div>
		</div>	

		<div class="col-md-6">
			<div class="form-group">
		    <label>Class de l'ID du produit</label>
		    {!! Form::text('id_produit', null, ['class' => 'form-control',  'placeholder'=>'Classname']) !!}
		  </div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
		    <label>Class du lien du produit</label>
		    {!! Form::text('url_class', null, ['class' => 'form-control',  'placeholder'=>'Classname']) !!}
		  </div>
		</div>


		<div class="col-md-6">
			<div class="form-group">
		    <label>Class du prix du produit</label>
		    {!! Form::text('prix', null, ['class' => 'form-control',  'placeholder'=>'Classname']) !!}
		  </div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
		    <label>Class de description du produit</label>
		    {!! Form::text('description', null, ['class' => 'form-control',  'placeholder'=>'Classname']) !!}
		  </div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
		    <label>Class du pays du produit</label>
		    {!! Form::text('country', null, ['class' => 'form-control',  'placeholder'=>'Classname']) !!}
		  </div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
		    <label>Class du region du produit</label>
		    {!! Form::text('region', null, ['class' => 'form-control',  'placeholder'=>'Classname']) !!}
		  </div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
		    <label>Class de cité du produit</label>
		    {!! Form::text('city', null, ['class' => 'form-control',  'placeholder'=>'Classname']) !!}
		  </div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
		    <label>Class des etoiles du produit</label>
		    {!! Form::text('stars', null, ['class' => 'form-control',  'placeholder'=>'Classname']) !!}
		  </div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
		    <label>Class du marque du produit</label>
		    {!! Form::text('brand', null, ['class' => 'form-control',  'placeholder'=>'Classname']) !!}
		  </div>
		</div>



		<div class="col-md-6">
			<div class="form-group">
		    <label>Class de période du produit</label>
		    {!! Form::text('periode', null, ['class' => 'form-control',  'placeholder'=>'Classname']) !!}
		  </div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
		    <label>Class de promotion du produit</label>
		    {!! Form::text('promotion', null, ['class' => 'form-control',  'placeholder'=>'Classname']) !!}
		  </div>
		</div>

		


		<div class="col-md-12">
			<div class="form-group text-right">
				<button class="btn btn-success">Enregistrer</button>
			</div>
		</div>


	</div>

{!! Form::close() !!}


@stop