@extends('layout')

@section('container')
		
<h1>La liste des produits</h1>
<div class="row">
<table class="table table-bordered table-striped">
	<thead>
		
	<tr>
		<th>Nom du produit</th>
		<th>Prix</th>
		<th>Description</th>
		<th>Période</th>
		<th>Etat</th>
		<th>ID produit</th>
		<th>Categorie</th>
		<th>Actions</th>
	</tr>
	</thead>
	<tbody>
		
		@foreach($produits as $produit)
		<tr>
			 
			
			<td><a href="{{$produit->url}}">{{$produit->name}}</a></td>
			<td>{{$produit->prix}}</td>
			<td>{{$produit->description}}</td>
			<td>{{$produit->periode}}</td>
					<td>
{!! Form::open(['url' => route('ajax-activate-produit'), 'class'=>'produit-form']) !!}
			<input type="hidden" name="id" value="{{$produit->id}}">
			<input type="hidden" name="type" value="{{$category}}">

<div class="onoffswitch">
    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch{{$produit->id}}" @if($produit->validate == 'true') checked @endif>
    <label class="onoffswitch-label" for="myonoffswitch{{$produit->id}}">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>
{!! Form::close() !!}
		</td>
			<td>{{$produit->produit_id}}</td>
			<td>{{$produit->category->titre}}</td>
			
			<td><a href=" {{route('show-product', [$produit->category->type,$produit->id])}}"> Détails</a></td>
		</tr>
		@endforeach
	</tbody>
</table>
</div>
@stop



@section('script')
<script type="text/javascript">
$('body').on('submit','.produit-form', function(e){
	e.preventDefault();
	var data = $(this).serialize();

   $.ajax({
     type : "post",
     dataType : "json",
     url : "{{route('ajax-activate-produit') }}",
     data :  data,
     success: function(infos) {
     	 
     	if(infos != true){
     		alert("Une erreur s'est produite, merci de contacter l'administateur");
     	}
      }
  });  
  });

$('body').on('change','.produit-form .onoffswitch-checkbox', function(){
  $(this).parent().parent().trigger('submit');
})
</script>
@stop