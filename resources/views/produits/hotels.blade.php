@extends('layout')

@section('container')

	
	{!! Form::model($produit,['url' => route('edit-product', [$produit->category->type, $produit->id]), 'files'=>true]) !!}

 	<h1>Modifier un produit :  <span>{{$produit->name}}</span></h1>
 	
	<div class="row">
 
		@foreach($hotel_attributs as $attr)
		<div class="col-md-6">
			<div class="form-group">
		    <label>{{$attr}}</label>
		    {!! Form::text($attr, null, ['class' => 'form-control',  'placeholder'=>$attr]) !!}
		  </div>
		</div>
		@endforeach
		<div class="col-md-6">
			<div class="form-group">
		    <label>Ajouter des images</label>
			<input type="file" name="image" class="form-control">
		  </div>
		</div>
</div>
<hr>
<h4>Liste des images</h4>
<div class="row">

		@foreach($produit->images as $image)
			<div class="col-md-4">
				

				<div class="img-responsive">
					<a href="#" class="delete-item" data-id="{{$image->id}}">x</a>
				<a  href="#" data-lity data-lity-target="{{asset('img/themes').'/'.$image->local}}"><img src="{{asset('img/themes').'/'.$image->local}}"></a>
				<label class="container-check"> Valider
				  <input type="checkbox" @if($image->validate == 'true') checked @endif name="photo[{{$image->id}}]">
				  <span class="checkmark"></span>
				</label>
				</div>

			</div>
		@endforeach
</div>
<div class="row">
		<div class="col-md-12">
			<div class="form-group text-right">
				<button class="btn btn-success">Enregistrer</button>
			</div>
		</div>


	</div>

{!! Form::close() !!}


@stop

@section('script')
<script type="text/javascript">
$('body').on('click','.delete-item', function(e){
	e.preventDefault();
	var result = confirm("Want to delete?");
	if (result) {
	     

		var id = $(this).data('id');
		$(this).parent().parent().remove();
		
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
	    
	    $.ajax({

	     type : "post",
	     dataType : "json",
	     url : "{{route('delete-img') }}",
	     data :  'id='+id,
	     success: function(infos) {
	     	 console.log(infos)
	     	if(infos != true){
	     		alert("Une erreur s'est produite, merci de contacter l'administateur");
	     	}
	      }
	  });  
    }
  });

$('body').on('change','.produit-form .onoffswitch-checkbox', function(){
  $(this).parent().parent().trigger('submit');
})
</script>
@stop