@extends('layout')

@section('container')
		
<h1>La liste des catégories</h1>
<div class="row">
<table class="table table-bordered table-striped">
	<thead>
		
	<tr>
		<th>Nom de la categorie</th>
		<th>Type</th>
		<th>Etat</th>
		<th>Configuration</th>
		<th>Produits</th>
	</tr>
	</thead>
	<tbody>
	@foreach($categories as $category)
	<tr>
		<td>{{$category->titre}}</td>
		<td>{{$type_category[$category->type]}}</td>
		<td>
			{!! Form::open(['url' => route('ajax-activate-category'), 'class'=>'category-form']) !!}
			<input type="hidden" name="id" value="{{$category->id}}">
			<div class="onoffswitch">
			    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch{{$category->id}}" @if($category->validate == 'true') checked @endif>
			    <label class="onoffswitch-label" for="myonoffswitch{{$category->id}}">
			        <span class="onoffswitch-inner"></span>
			        <span class="onoffswitch-switch"></span>
			    </label>
			</div>


			{!! Form::close() !!}
		</td>
		<td><a href=" {{route('edit-category', $category->id)}}">Configurer</a></td>
		<td><a href=" {{route('all-products', [$category->type, $category->id])}}">Liste des produits</a></td>
	</tr>
	@endforeach
</tbody>
</table>
</div>
@stop

@section('script')
<script type="text/javascript">
$('body').on('submit','.category-form', function(e){
	e.preventDefault();
	var data = $(this).serialize();

   $.ajax({
     type : "post",
     dataType : "json",
     url : "{{route('ajax-activate-category') }}",
     data :  data,
     success: function(infos) {
     	 
     	if(infos != true){
     		alert("Une erreur s'est produite, merci de contacter l'administateur");
     	}
      }
  });  
 });

$('body').on('change','.category-form .onoffswitch-checkbox', function(){
  $(this).parent().parent().trigger('submit');
})
</script>
@stop