@extends('layout')

@section('container')

{!! Form::open(['url' => route('crawlertest'), 'files'=>true]) !!}

 	<h1>Tester un crawler</h1>
 	<div class="alert alert-success">
 		<p>Class :  find, children , exemple : find('#table-id21 tr')[0]->children(1)->children(0)->innertext</p>
 		<p>Type : action, href, innertext, outertext, <a href="http://simplehtmldom.sourceforge.net/manual.htm" target="_blank">voir documentation</a></p>
 	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
		    <label>Lien</label>
		    {!! Form::text('url', $url, ['class' => 'form-control', 'required'=>true, 'placeholder'=>'Lien']) !!}
		  </div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
		    <label>Class name</label>
		    {!! Form::text('classname', $classname, ['class' => 'form-control', 'required'=>true, 'placeholder'=>'Classname']) !!}
		  </div>
		</div>

		

		

		<div class="col-md-2">
			<div class="form-group text-right">

				<button class="btn btn-success" style="margin-top: 30px">Enregistrer</button>
			</div>
		</div>


	</div>

{!! Form::close() !!}


@if(isset($response))
<div class="alert alert-info">
<p>Valeur :</p>
@if(!is_array($response))
{!! $response !!}
@else 
<pre>
	{!! var_dump($response) !!}
</pre>
@endif
</div>
@endif

@stop