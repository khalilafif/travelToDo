<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Htmldom;
use App\Category;
use Image;
use Input;

class CrawlerController extends Controller
{
	 
	
    public function test(){
		$url = null;
		$classname = null;
 		return view('crawler.test', compact('url', 'classname'));
    }

    public function crawlertest(Request $request){
    	$url = $request->url;
    	$classname = $request->classname;
    
    	$response = "";
		
		$dom = new Htmldom($url);


		$response = eval("return \$dom->".$classname.";");
	 

		return view('crawler.test')
						->with('response',$response)
						->with('url',$url)
						->with('classname',$classname);

    }



  
}

