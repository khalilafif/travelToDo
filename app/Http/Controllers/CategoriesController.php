<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Htmldom;
use App\Category;
use App\Image as Img ;
use App\Destination;
use App\Hotel;
use Image;
use Input;

class CategoriesController extends Controller
{
	
	public $type_category ;
	public $type_destinantion ;

    public function allcategories($type){

    	$type_category = $this->type_category;
 		$type_produit = $this->type_produit;

		$categories = Category::where('type', $type)->get();
		return view('produits.categories', compact('categories', 'type_category', 'type_produit'));
	}
	public function createcategory(){
		$type_category = $this->type_category;
 		$type_produit = $this->type_produit;
		return view('produits.categories-form', compact('type_category', 'type_produit'));
	}
	public function savecategory(Request $request){
		$category = new Category;
		$category->titre = $request->titre;
		$category->url_class = $request->url_class;
		$category->validate = 'false';
		 
	 
		$category->type = $request->type;
		$category->html = $request->html;
		$category->name = $request->name;
		$category->url = $request->url;
		$category->prix = $request->prix;
		$category->description = $request->description;
		$category->periode = $request->periode;

		$category->image = $request->image;
		$category->id_produit = $request->id_produit;

		$category->address = $request->address;
		$category->city = $request->city;
		$category->region = $request->region;
		$category->code_postal = $request->code_postal;
		$category->country = $request->country;
		$category->neighborhood = $request->neighborhood;
		$category->brand = $request->brand;
		$category->stars = $request->stars;
		$category->latitude = $request->latitude;
		$category->longitude = $request->longitude;
		 
		$category->promotion = $request->promotion;
		
		if(Input::file('theme')){
  			$image = Input::file('theme');
            $filename  = time() .'.' . $image->getClientOriginalExtension();

            $path = public_path('/img/uploads/').'/'. $filename;
            Image::make($image)->save($path, 80);
            $category->theme = $filename;
         
       }

       
		$category->save();

		return redirect()->back();

	}

	public function editcategory($id){

		$category = Category::findOrFail($id);
		return $this->createcategory()->with('category', $category);
	}
	public function updatecategory(Request $request, $id){

		$category = Category::findOrFail($id);
		$category->name = $request->name;
		$category->url_class = $request->url_class;
		
		//$category->validate = 'fa';
		 
	 
		$category->type = $request->type;
		$category->html = $request->html;
		$category->image = $request->image;
		$category->id_produit = $request->id_produit;
		$category->name = $request->name;
		$category->url = $request->url;
		$category->prix = $request->prix;
		$category->description = $request->description;
		$category->periode = $request->periode;
		$category->promotion = $request->promotion;
		$category->address = $request->address;
		$category->city = $request->city;
		$category->region = $request->region;
		$category->code_postal = $request->code_postal;
		$category->country = $request->country;
		$category->neighborhood = $request->neighborhood;
		$category->brand = $request->brand;
		$category->stars = $request->stars;
		$category->latitude = $request->latitude;
		$category->longitude = $request->longitude;

		if(Input::file('theme')){
  			$image = Input::file('theme');
            $filename  = time() .'.' . $image->getClientOriginalExtension();

            $path = public_path('/img/uploads/').'/'. $filename;
            Image::make($image)->save($path, 80);
            $category->theme = $filename;
         
       }

       
		$category->update();
		return redirect()->back();
	
	}

	public function test($category){

		$page_detail = [
			'id_produit',
			'image',
			'promotion',
			'address',
			'city',
			'region',
			'code_postal',
			'country',
			'neighborhood',
			'brand',
			'stars',
		];

		$page_list = [
			'name',
			'url_class',
			'prix',
			'description',
			'periode',
			'promotion',
		];

		$category = Category::find($category);
		$html = new Htmldom($category->url);
		$classname = $category->html;
		$produits = array();
		$i = 0;

		//dd($category);
		
		foreach ($page_list as $attr){
			if(!empty($category->$attr)){

				$code = explode('[0]' ,$category->$attr);
				$find = $code[0];
				$action = $code[1];
				
				$response = eval("return \$html->".$find.";");


				foreach($response as $key=>$ligne){
					
				 	$codeaction = "\$ligne".$action;
				 	  
					$produits[$key][$attr] = strip_tags( eval("return $codeaction ;"));
					
				}
				
			}
			 
		}
		
		 
	 
		//parsing id and photos
		foreach ($produits as $key => $produit) {
			if($produit['url_class'] != false){
				//parsing page details by url class
				$produit_html = new Htmldom(htmlspecialchars_decode($produit['url_class']));

				//$code_id = '\$produit_html->'.$category->id_produit;
				$url_id =  eval("return \$produit_html->".$category->id_produit .";");
				 
				$parts = parse_url(htmlspecialchars_decode($url_id));
				parse_str($parts['query'], $query);
				$new = false;

				//the id
				$produits[$key]['produit_id'] = $query['id'];
				//the country region country  stars address city
				if(!empty($category->region))
					$produit['region'] = strip_tags(eval("return \$produit_html->".$category->region .";"));
				if(!empty($category->country))
					$produit['country'] = strip_tags(eval("return \$produit_html->".$category->country .";"));
				if(!empty($category->stars))
					$produit['stars'] = strip_tags(eval("return \$produit_html->".$category->stars .";"));
				if(!empty($category->address))
					$produit['address'] = strip_tags(eval("return \$produit_html->".$category->address .";"));
				if(!empty($category->city))
					$produit['city'] = strip_tags(eval("return \$produit_html->".$category->city .";"));


				//if hotel or destination get data
				if($category->type == 0){
					$new_produit = Hotel::where('produit_id',$query['id'])->get()->first();
				}else {
					$new_produit = Destination::where('produit_id',$query['id'])->get()->first();
				}
				
				 
				if(count($new_produit) == 0 && $category->type == 0){
					$new_produit = new Hotel;
					$new = true;
				}else if(count($new_produit) == 0 && $category->type == 1) {
					$new_produit = new Destination;
					$new = true;
				}
				
				if(isset($produit['name'])) $new_produit->name= $produit['name'];
				if(isset($produit['url_class'])) $new_produit->url = htmlspecialchars_decode($produit['url_class']);
				if(isset($produit['prix'])) $new_produit->prix = $produit['prix'];
				if(isset($produit['description'])) $new_produit->description = $produit['description'];
				if(isset($produit['periode'])) $new_produit->periode = $produit['periode'];
				if(isset($produit['promotion'])) $new_produit->promotion = $produit['promotion'];
				if(isset($produit['country'])) $new_produit->country = $produit['country'];
				if(isset($produit['region'])) $new_produit->region = $produit['region'];
				//if(isset($produit['city'])) $new_produit->city = $produit['city'];
				if(isset($produit['address'])) $new_produit->address = $produit['address'];
				if(isset($produit['stars'])) $new_produit->stars = $produit['stars'];
				if(isset( $query['id'])) $new_produit->produit_id = $query['id'];
				$new_produit->category_id = $category->id;

			 
				if($new){
					$new_produit->save();
				}else {
					$new_produit->update();
				}

				//dd($category->image, $produit_html->find($category->image) );
				//images 
				if(!empty($category->image)){

					$code_img = explode('[0]', $category->image);
					$find = eval("return \$produit_html->".$code_img[0].";");
					
					foreach($find as $key=>$image){
						$src = eval("return \$image".$code_img[1].";");
						
						$old  = Img::where('url',$find)->count();
						if($old ==0 && $image->src != false){
							$img = new Img;
							$img->url = $image->src;
							$img->validate = 'false';
							if($category->type == 1){
								$img->destination_id = $new_produit->id;
							}else {
								$img->hotel_id = $new_produit->id;
							}
							$img->local = $this->buildImage($image->src, $category->theme, $new_produit->prix, $new_produit->name, $new_produit->description, $key);
							 
							$img->save();
						}
					}
				}
			}

		}

	 	if($category->type == 0){
	 		dd(Hotel::with('images')->get());
	 	}else {
	 		dd(Destination::with('images')->get());

	 	}


		
	}

	/*
	* 	$url : base image from traveltodo
	*	$theme : theme
	*	
	*/
	public function buildImage($url, $theme, $prix, $titre, $description, $key){
		$url_theme = url("/img/uploads/").'/'.$theme;
        
        $arrContextOptions=array(
        "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );  
     
        $data = file_get_contents($url, false, stream_context_create($arrContextOptions));
        $name = $key.'-'.time().'.jpg';
        $fileName =public_path('/img/uploads/').$name  ;
        $file = fopen($fileName, 'w+');
        fputs($file, $data);
        fclose($file);

        $prix = trim(str_replace('à partir de', '', $prix));

        $image =  Image::make(url('img/uploads').'/'.$name);
        $image->fit(800, 500);
        $new_image =  new Image;
        $new_image->make($image)
        ->insert($url_theme, 'top-left', 0, 0)
        ->text(wordwrap($titre, 30, "\n"), 20, 170, function($font) {
            $font->file(public_path('/fonts/Ubuntu-MI.ttf'));
            $font->size(20);
            $font->color('#2a94b7');
            $font->align('left');
            $font->valign('top');
        })
        ->text($prix, 20, 300, function($font) {
            $font->file(public_path('/fonts/Ubuntu-MI.ttf'));
            $font->size(50);
            $font->color('#f42969');
            $font->align('left');
            $font->valign('top');
        })
        ->save(public_path('/img/').'themes/product-'.$key.'-'.time() .'.jpg', 100);

        return 'product-'.$key.'-'.time() .'.jpg';

	}

	public function stateCategory(Request $request){
		 
		$category = Category::findOrFail($request->id);
		if($request->onoffswitch){
			$category->validate = "true";
		}else {
			$category->validate = "false";
		}
		$category->update();
		 
		return 'true';

	}
}
