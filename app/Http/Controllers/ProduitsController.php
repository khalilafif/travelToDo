<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Htmldom;
use App\Destination;
use App\Category;
use App\Image as Img ;
use App\Hotel;
use Image;
use Input;
use Excel;

class ProduitsController extends Controller
{
	protected $hotel_attributs = array();
	protected $destination_attributs = array();

	public $type_category ;
	public $type_destinantion ;

	public function __construct(){
		$this->hotel_attributs = [
    		'name',
			'url',
			'prix',
			'description',
			'periode',
			'date',
			'promotion',
			'address',
			'city',
			'region',
			'code_postal',
			'country',
			'neighborhood',
			'brand',
			'stars',
			'latitude',
			'longitude',
    	];

    	$this->destination_attributs = [
    		'name',
			'url',
			'prix',
			'description',
			'periode',
			'date',
			'promotion',
			'country',
			'address',
			'city',
			'region',
			'produit_id',
    	];

	}
    public function index($category, $id){
    	if($category == 0){
    		$produits = Hotel::with('category')->where('category_id', $id)->get();
    	}
		else {
    		$produits = Destination::with('category')->where('category_id', $id)->get();
		}
    	
    	return view('produits.produits', compact('produits', 'category'));
    }

    public function show($type, $id){

    	$hotel_attributs = $this->hotel_attributs;
		$destination_attributs = $this->destination_attributs;

    	if($type == 0){
    		$produit = Hotel::with('images')->findOrFail($id);
    		return view('produits.hotels', compact('produit', 'hotel_attributs', 'destination_attributs'));
    	}else {
    		$produit = Destination::with('images')->findOrFail($id);
    		return view('produits.hotels', compact('produit', 'hotel_attributs', 'destination_attributs'));
    	}


    }

    public function edit(Request $request, $category, $id){

		if($category == 0){
    		$produit = Hotel::findOrFail($id);
    		$attributs = $this->hotel_attributs;
    	}
		else {
    		$produit= Destination::findOrFail($id);
			$attributs = $this->destination_attributs;
		}

		foreach ($attributs as  $attr) {
			$produit->$attr = $request->$attr;
		}
		$produit->update();


		if(Input::file('image')){
  			$image = Input::file('image');
            $filename  = time() .'.' . $image->getClientOriginalExtension();

            $path = public_path('/img/themes/').'/'. $filename;
            Image::make($image)->fit(800,500)->save($path, 80);
            
            $img = new Img;
			$img->url = "";
			$img->local = $filename;
			$img->validate = 'true';
			if($category == 0){
				$img->hotel_id = $produit->id;
			}else {
				$img->destination_id =$produit->id;
			}
			$img->save();
       	}

       	$images = $produit->images()->get();
       	foreach($images as $img){
       		 
       		if(isset($request->photo[$img->id])){
       			$img->validate = 'true';
       			$img->update();
       		}
       	}
       
       	return redirect()->back();
    	 
    }

    public function export($category){
    	$filename = 'export-hotel-'.time();
   		$type = ['hotels', 'destinations'];
 



 		$hotel_attr = [
 			'produit_id'=>'hotel_id',
 			'url'=>'url',
 			'name'=>'name',
 			'description'=>'description',
 			'brand'=>'brand',
 			'address'=>'address.addr1',
 			'city'=>'address.city',
 			'region'=>'address.region',
 			'country'=>'address.country',
 			'latitude'=>'latitude',
 			'longitude'=>'longitude',
 			'neighborhood'=>'neighborhood[0]',
 			'prix'=>'custom_label_0', 
 			'price'=>'base_price',
 			'stars'=>'star_rating',
 			 
 		];
    	if($category==1){
            $products = Category::where('validate', 'true')
                                ->where('type', $category)
                                ->with([$type[$category]=>function($query){
                                    $query->with('images');
                                }])->get();
        }
        else{
            $products = Category::where('validate', 'true')
                                ->where('type', $category)
                                ->with([$type[$category]=>function($query){
                                    $query->where('validate', 'true')
                                          ->with('images');
                                }])->get();
        }

    	$data = array();

    	foreach ($products as  $cat) {
            $var  = $type[$category];
    		foreach ($cat->$var as $key=>$produit) {
    			foreach($hotel_attr as $k=>$attr){
    				$produit->price = $produit->prix;
    				$val =  trim(str_replace(['à partir de', 'dt', 'étoiles'], ['', '', ''],$produit->$k));
    				if($k == 'price'){
    					$data[$key][$k] = $val.' TND';
    				}else {
    					$data[$key][$k] =$val;
    				}
    			}
    			foreach ($produit->images as $i=>$img) {
    				if($img->validate ==  'true'){
    					$data[$key]['image['.$i.'].url']  = asset('img/theme').'/'.$img->local;
    				}
    			}
    		}
    	}
    	$header[0] = $hotel_attr;
    	for($i = 0; $i<=19; $i++){
    		$header[0]['image-'.$i] = 'image['.$i.'].url';
    	}
    	$content = array_merge($header, $data);
    	 
    	//dd($content);
    	Excel::create($filename, function($excel) use ($content)  {

		    $excel->sheet('Data', function($sheet) use ($content) {
		    	$sheet->setPageMargin(array(
				    0.25, 0.30, 0.25, 0.30
				)); 
		        $sheet->fromArray($content, null, 'A1', false, false);
		       

		    });

		})->export('csv');
    }

    public function stateProduit(Request $request){
        if($request->type == 0){
            $produit = Hotel::findOrFail($request->id);
        }else {
            $produit = Destination::findOrFail($request->id);
        }
        if($request->onoffswitch){
            $produit->validate = "true";
        }else {
            $produit->validate = "false";
        }
        $produit->update();
         
        return 'true';
    }

    public function deleteImage(Request $request){
         
        $img = Img::findOrFail($request->id);
        $img->delete();
        return 'true';
    }
}
