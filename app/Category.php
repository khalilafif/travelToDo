<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function destinations()
    {
        return $this->hasMany('App\Destination');
    }
    public function hotels()
    {
        return $this->hasMany('App\Hotel');
    }
}
