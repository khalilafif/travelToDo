<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable();
            $table->text('url')->nullable();
            $table->string('prix')->nullable();
            $table->text('description')->nullable();
            $table->string('periode')->nullable();
            $table->string('date')->nullable();
            $table->string('promotion')->nullable();
            $table->text('address')->nullable();
            $table->string('city')->nullable();
            $table->string('region')->nullable();
            $table->string('code_postal')->nullable();
            $table->string('country')->nullable();
            $table->text('neighborhood')->nullable();
            $table->string('brand')->nullable();
            $table->string('stars')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();

            $table->string('validate')->default('false');

            //sur le site
            $table->string('produit_id')->nullable();
           

            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
           


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
