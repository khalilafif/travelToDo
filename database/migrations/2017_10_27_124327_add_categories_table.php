<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            //category settings
            $table->string('titre');
            $table->string('url');
            $table->string('theme');
            

            //produits settings
            $table->string('html')->nullable();
            $table->string('id_produit')->nullable();
            $table->string('name')->nullable();
            $table->string('url_class')->nullable();
            $table->string('prix')->nullable();
            $table->string('description')->nullable();
            $table->string('periode')->nullable();
            $table->string('image')->nullable();
            $table->string('promotion')->nullable();

            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('region')->nullable();
            $table->string('code_postal')->nullable();
            $table->string('country')->nullable();
            $table->string('neighborhood')->nullable();
            $table->string('brand')->nullable();
            $table->string('stars')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();


            $table->string('validate')->default('false');

            $table->string('type');//hotel ou destination

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
