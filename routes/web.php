<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/test-crawler', ['uses'=>'CrawlerController@test'])->name('test');
Route::post('/crawler-test', ['uses'=>'CrawlerController@crawlertest'])->name('crawlertest');

Route::get('/crawler-index', ['uses'=>'CrawlerController@index']);
Route::get('/add-category', ['uses'=>'CategoriesController@createcategory'])->name('add-category');
Route::get('/categories/{type}', ['uses'=>'CategoriesController@allcategories'])->name('all-categories');
Route::get('/edit-category/{id}', ['uses'=>'CategoriesController@editcategory'])->name('edit-category');
Route::post('/update-category/{id}', ['uses'=>'CategoriesController@updatecategory'])->name('update-category');
Route::post('/save-category', ['uses'=>'CategoriesController@savecategory'])->name('save-category');

Route::get('/test/{id}', ['uses'=>'CategoriesController@test'])->name('test-category');

Route::get('/all-products/{category}/{id}', ['uses'=>'ProduitsController@index'])->name('all-products');
Route::get('/product/{type}/{id}', ['uses'=>'ProduitsController@show'])->name('show-product');
Route::post('/edit-product/{type}/{id}', ['uses'=>'ProduitsController@edit'])->name('edit-product');

Route::get('/export/{type}', ['uses'=>'ProduitsController@export'])->name('export');


//activate on off category
Route::post('/state-category', ['uses'=>'CategoriesController@stateCategory'])->name('ajax-activate-category');
//activate on off produit
Route::post('/state-produit', ['uses'=>'ProduitsController@stateProduit'])->name('ajax-activate-produit');
//delete img
Route::post('/delete-img', ['uses'=>'ProduitsController@deleteImage'])->name('delete-img');
 
 
 